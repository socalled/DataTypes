/*
  Copyright (C) 2023 Kirill Chuvilin, <k.chuvilin@socalled.link>

  This file is a part of the SoCalled Framework.

  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at https://mozilla.org/MPL/2.0/.
*/

#include <SoCalledTypeInfo.h>

using namespace SoCalled;

const char * formatTemplate = "%%-19s%%-8%s%%-14%s%%-8%s%%-14%s%%%s\n";
char format[40];
char buffer[100];

template <typename T> void printTypeInfo(const T &value) {
    const TypeInfo typeInfo(value);
    static char buffer[80];
    sprintf(format, formatTemplate, "u", "p", "p", "u", "u");
    sprintf(buffer, format,
            typeInfo.name, typeInfo.size, value, typeInfo.null, typeInfo.isPointer, typeInfo.isFundamental);
    Serial.print(buffer);
}

class DeclaredClass { };
DECLARE_TYPE_INFO(DeclaredClass)

class NotDeclaredClass { };
    
void setup() {
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, HIGH);

    Serial.begin(9600);
    delay(1000);
    Serial.println();

    sprintf(format, formatTemplate, "s", "s", "s", "s", "s");
    sprintf(buffer, format, "Type", "Size", "Value", "Null", "Is pointer", "Is fundamental");
    Serial.print(buffer);

                                                     printTypeInfo(nullptr);
    auto pVoid             = malloc(1);              printTypeInfo(pVoid);
    auto pInt              = new int();              printTypeInfo(pInt);
    auto pDeclaredClass    = new DeclaredClass();    printTypeInfo(pDeclaredClass);
    auto pNotDeclaredClass = new NotDeclaredClass(); printTypeInfo(pNotDeclaredClass);
    
    digitalWrite(LED_BUILTIN, LOW);
}

void loop() {
}
