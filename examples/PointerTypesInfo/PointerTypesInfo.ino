/*
  Copyright (C) 2023 Kirill Chuvilin, <k.chuvilin@socalled.link>

  This file is a part of the SoCalled Framework.

  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at https://mozilla.org/MPL/2.0/.
*/

#include <SoCalledTypeInfo.h>

using namespace SoCalled;

const char * formatTemplate = "%%-19s%%-8%s%%-8%s%%-14%s%%%s\n";
char format[40];
char buffer[100];

template <typename T> void printTypeInfo() {
    // Static TypeInfo<T> properties can be used instead of macros.
    sprintf(format, formatTemplate, "u", "p", "u", "u");
    sprintf(buffer, format,
            TYPE_NAME(T), TYPE_SIZE(T), TYPE_NULL(T), POINTER_TYPE(T), FUNDAMENTAL_TYPE(T));
    Serial.print(buffer);
}

class DeclaredClass { };
DECLARE_TYPE_INFO(DeclaredClass)

class NotDeclaredClass { };
    
void setup() {
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, HIGH);

    Serial.begin(9600);
    delay(1000);
    Serial.println();

    sprintf(format, formatTemplate, "s", "s", "s", "s");
    sprintf(buffer, format, "Type", "Size", "Null", "Is pointer", "Is fundamental");
    Serial.print(buffer);

    printTypeInfo<decltype(nullptr)>();
    printTypeInfo<void *>();
    printTypeInfo<int *>();
    printTypeInfo<DeclaredClass *>();
    printTypeInfo<NotDeclaredClass *>();
    
    digitalWrite(LED_BUILTIN, LOW);
}

void loop() {
}
