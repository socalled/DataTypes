/*
  Copyright (C) 2023 Kirill Chuvilin, <k.chuvilin@socalled.link>

  This file is a part of the SoCalled Framework.

  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at https://mozilla.org/MPL/2.0/.
*/

#include <SoCalledTypeInfo.h>

using namespace SoCalled;

const char * formatTemplate = "%%-22s%%-8%s%%-24%s%%-24%s%%%s\n";
char format[40];
char buffer[100];

template <typename T> void printTypeInfo(const T &value, const char * formatSpecifier) {
    const TypeInfo typeInfo(value);
    sprintf(format, formatTemplate, "u", formatSpecifier, formatSpecifier, formatSpecifier);
    sprintf(buffer, format, typeInfo.name, typeInfo.size, typeInfo.null, typeInfo.min, typeInfo.max);
    Serial.print(buffer);
}

void setup() {
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, HIGH);

    Serial.begin(9600);
    delay(1000);
    Serial.println();

    sprintf(format, formatTemplate, "s", "s", "s", "s");
    sprintf(buffer, format, "Type", "Size", "Null", "Min", "Max");
    Serial.print(buffer);

    signed char        signedCharValue       = 0; printTypeInfo(signedCharValue, "d");
    unsigned char      unsignedCharValue     = 0; printTypeInfo(unsignedCharValue, "u");
    char               charValue             = 0; printTypeInfo(charValue, TypeInfo<char>::min < 0 ? "d" : "u");
    byte               byteValue             = 0; printTypeInfo(byteValue, "u");
    
    short              shortValue            = 0; printTypeInfo(shortValue, "d");
    unsigned short     unsignedShortValue    = 0; printTypeInfo(unsignedShortValue, "u");
    word               wordValue             = 0; printTypeInfo(wordValue, "u");

    int                intValue              = 0; printTypeInfo(intValue, "d");
    unsigned int       unsignedIntValue      = 0; printTypeInfo(unsignedIntValue, "u");

    long               longValue             = 0; printTypeInfo(longValue, "ld");
    unsigned long      unsignedLongValue     = 0; printTypeInfo(unsignedLongValue, "lu");

    // Note: Arduino's implementation of sprintf doesn't work with %lld, %llu, %e

    long long          longLongValue         = 0; printTypeInfo(longLongValue, "lld");
    unsigned long long unsignedLongLongValue = 0; printTypeInfo(unsignedLongLongValue, "llu");

    float              floatValue            = 0; printTypeInfo(floatValue, "e");
    double             doubleValue           = 0; printTypeInfo(doubleValue, "e");

    digitalWrite(LED_BUILTIN, LOW);
}

void loop() {
}
