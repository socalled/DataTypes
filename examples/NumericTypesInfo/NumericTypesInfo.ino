/*
  Copyright (C) 2023 Kirill Chuvilin, <k.chuvilin@socalled.link>

  This file is a part of the SoCalled Framework.

  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at https://mozilla.org/MPL/2.0/.
*/

#include <SoCalledTypeInfo.h>

using namespace SoCalled;

const char * formatTemplate = "%%-22s%%-8%s%%-24%s%%-24%s%%%s\n";
char format[40];
char buffer[100];

template <typename T> void printTypeInfo(const char * formatSpecifier) {
    // Static TypeInfo<T> properties can be used instead of macros.
    sprintf(format, formatTemplate, "u", formatSpecifier, formatSpecifier, formatSpecifier);
    sprintf(buffer, format, TYPE_NAME(T), TYPE_SIZE(T), TYPE_NULL(T), TYPE_MIN(T), TYPE_MAX(T));
    Serial.print(buffer);
}

void setup() {
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, HIGH);

    Serial.begin(9600);
    delay(1000);
    
    sprintf(format, formatTemplate, "s", "s", "s", "s");
    sprintf(buffer, format, "Type", "Size", "Null", "Min", "Max");
    Serial.print(buffer);

    printTypeInfo<signed char>("d");
    printTypeInfo<unsigned char>("u");
    printTypeInfo<char>(TYPE_MIN(char) < 0 ? "d" : "u");
    printTypeInfo<byte>("u");

    printTypeInfo<short>("d");
    printTypeInfo<unsigned short>("u");
    printTypeInfo<word>("u");

    printTypeInfo<int>("d");
    printTypeInfo<unsigned int>("u");

    printTypeInfo<long>("ld");
    printTypeInfo<unsigned long>("lu");

    // Note: Arduino's implementation of sprintf doesn't work with %lld, %llu, %e

    printTypeInfo<long long>("lld");
    printTypeInfo<unsigned long long>("llu");

    printTypeInfo<float>("e");
    printTypeInfo<double>("e");

    digitalWrite(LED_BUILTIN, LOW);
}

void loop() {
}
