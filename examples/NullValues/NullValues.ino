/*
  Copyright (C) 2023 Kirill Chuvilin, <k.chuvilin@socalled.link>

  This file is a part of the SoCalled Framework.

  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at https://mozilla.org/MPL/2.0/.
*/

#include <SoCalledTypeInfo.h>

using namespace SoCalled;

const char * formatTemplate = "%%-19s%%-16%s%%-15%s%%%s\n";
char format[40];
char buffer[100];

template <typename T> void printNullStates(const T &value, const char * formatSpecifier) {
    const TypeInfo typeInfo(value);
    sprintf(format, formatTemplate, formatSpecifier, formatSpecifier, "u");
    sprintf(buffer, format, typeInfo.name, value, typeInfo.null, typeInfo.isNull());
    Serial.print(buffer);
}

class DeclaredClass { };
DECLARE_TYPE_INFO(DeclaredClass)

class NotDeclaredClass { };

void setup() {
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, HIGH);

    Serial.begin(9600);
    delay(1000);
    Serial.println();

    sprintf(format, formatTemplate, "s", "s", "s");
    sprintf(buffer, format, "Type", "Value", "Null", "Is null");
    Serial.print(buffer);

                                                                    printNullStates(nullptr, "p");
    void             * pVoid              = nullptr;                printNullStates(pVoid, "p");
    int              * pInt               = nullptr;                printNullStates(pInt, "p");
    DeclaredClass    * pDeclaredClass     = nullptr;                printNullStates(pDeclaredClass, "p");
                       pDeclaredClass     = new DeclaredClass();    printNullStates(pDeclaredClass, "p");
    NotDeclaredClass * pNotDeclaredClass  = nullptr;                printNullStates(pNotDeclaredClass, "p");
                       pNotDeclaredClass  = new NotDeclaredClass(); printNullStates(pNotDeclaredClass, "p");

    signed char        signedCharValue    = 0;                      printNullStates(signedCharValue, "d");
    unsigned char      unsignedCharValue  = 0;                      printNullStates(unsignedCharValue, "u");
    char               charValue          = 0;                      printNullStates(charValue, TYPE_MIN(char) < 0 ? "d" : "u");
    byte               byteValue          = 0;                      printNullStates(byteValue, "u");
    
    short              shortValue         = 0;                      printNullStates(shortValue, "d");
    unsigned short     unsignedShortValue = 0;                      printNullStates(unsignedShortValue, "u");
    word               wordValue          = 0;                      printNullStates(wordValue, "u");

    int                intValue           = 0;                      printNullStates(intValue, "d");
    unsigned int       unsignedIntValue   = 0;                      printNullStates(unsignedIntValue, "u");

    long               longValue          = 0;                      printNullStates(longValue, "ld");
    unsigned long      unsignedLongValue  = 0;                      printNullStates(unsignedLongValue, "lu");

    float              floatValue         = 0;                      printNullStates(floatValue, "e");
    double             doubleValue        = 0;                      printNullStates(doubleValue, "e");

    digitalWrite(LED_BUILTIN, LOW);
}

void loop() {
}
