/*
  Copyright (C) 2023 Kirill Chuvilin, <k.chuvilin@socalled.link>

  This file is a part of the SoCalled Framework.

  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file,
  You can obtain one at https://mozilla.org/MPL/2.0/.
*/

#pragma once

#include <limits.h>
#include <float.h>

namespace SoCalled {

#define FUNDAMENTAL_TYPE(T) TypeInfo<T>::isFundamental
#define INTEGER_TYPE(T)     TypeInfo<T>::isInteger
#define NUMERIC_TYPE(T)     TypeInfo<T>::isNumeric
#define POINTER_TYPE(T)     TypeInfo<T>::isPointer
#define TYPE_MAX(T)         TypeInfo<T>::max
#define TYPE_MIN(T)         TypeInfo<T>::min
#define TYPE_NAME(T)        TypeInfo<T>::name
#define TYPE_NULL(T)        TypeInfo<T>::null
#define TYPE_SIZE(T)        TypeInfo<T>::size

#define IS_NULL(value) TypeInfo(value).isNull()

template <typename T>
class TypeInfo
{
public:
    TypeInfo(const T &value) { }

    static const bool isFundamental = false;
    static const bool isInteger = false;
    static const bool isNumeric = false;
    static const bool isPointer = false;
    static constexpr const char * name = "";
    static const int size = sizeof(T);
};

template<>
class TypeInfo<void>
{
public:
    TypeInfo(void) { }

    static const bool isFundamental = false;
    static const bool isInteger = false;
    static const bool isNumeric = false;
    static const bool isPointer = false;
    static constexpr const char * name = "void";
    static const int size = 0;
};

template<>
class TypeInfo<bool>
{
public:
    TypeInfo(bool) { }

    static const bool isFundamental = true;
    static const bool isInteger = false;
    static const bool isNumeric = false;
    static const bool isPointer = false;
    static constexpr const char * name = "bool";
    static const int size = sizeof(bool);
};

template<>
class TypeInfo<decltype(nullptr)>
{
public:
    TypeInfo(decltype(nullptr)) { }
    bool isNull() const { return true; }

    static const bool isFundamental = true;
    static const bool isInteger = false;
    static const bool isNumeric = false;
    static const bool isPointer = true;
    static constexpr const char * name = "nullptr_t";
    static constexpr auto null = nullptr;
    static const int size = sizeof(decltype(nullptr));
};

template<int size> struct TypeNameBuffer
{
    char data[size];
};

template<typename T> constexpr auto pointerTypeNameBuffer = [] {
    constexpr auto typeNameLength = [] {
        int length = 0;
        while (TypeInfo<T>::name[length]) ++length;
        return length;
    }();

    constexpr auto pointerTypeNameLength = typeNameLength > 0 ? typeNameLength + 2 : 1;

    TypeNameBuffer<pointerTypeNameLength + 1> nameBuffer { };

    if (typeNameLength > 0) {
        for (int iChar = 0; iChar < typeNameLength; ++iChar)
            nameBuffer.data[iChar] = TypeInfo<T>::name[iChar];
        nameBuffer.data[pointerTypeNameLength - 2] = ' ';
    }

    nameBuffer.data[pointerTypeNameLength - 1] = '*';
    nameBuffer.data[pointerTypeNameLength] = '\0';

    return nameBuffer;
}();

template<typename T>
class TypeInfo<T *>
{
public:
    TypeInfo(const T *value) : m_value(value) { }
    bool isNull() const { return m_value == nullptr; }

    static const bool isFundamental = false;
    static const bool isInteger = false;
    static const bool isNumeric = false;
    static const bool isPointer = true;
    static constexpr const char * name = pointerTypeNameBuffer<T>.data;
    static constexpr T * null = nullptr;
    static const int size = sizeof(T *);

private:
    const T * m_value;
};

#define DECLARE_TYPE_INFO(T) \
    namespace SoCalled { \
        template<> class TypeInfo<T> \
        { \
        public: \
            TypeInfo(const T &value) { } \
            \
            static const bool isFundamental = false; \
            static const bool isInteger = false; \
            static const bool isNumeric = false; \
            static const bool isPointer = false; \
            static constexpr const char * name = #T; \
            static const int size = sizeof(T); \
        }; \
    }

#define DECLARE_NULLABLE_TYPE_INFO(T, null_) \
    namespace SoCalled { \
        template<> \
        class TypeInfo<T> \
        { \
        public: \
            TypeInfo(const T &value) : m_value(value) { } \
            bool isNull() const { return m_value == null_; } \
            \
            static const bool isFundamental = false; \
            static const bool isInteger = false; \
            static const bool isNumeric = false; \
            static const bool isPointer = false; \
            static constexpr const char * name = #T; \
            static constexpr T null = null_; \
            static const int size = sizeof(T); \
            \
            private: \
                const T &m_value; \
        }; \
    }

#define DECLARE_NUMERIC_TYPE_INFO(T, isInteger_, null_, min_, max_) \
    namespace SoCalled { \
        template<> \
        class TypeInfo<T> \
        { \
        public: \
            TypeInfo(const T &value) : m_value(value) { } \
            bool isNull() const { return m_value == null_ || m_value != m_value; } \
            \
            static const bool isFundamental = true; \
            static const bool isInteger = isInteger_; \
            static const bool isNumeric = true; \
            static const bool isPointer = false; \
            static constexpr T max = max_; \
            static constexpr T min = min_; \
            static constexpr const char * name = #T; \
            static constexpr T null = null_; \
            static const int size = sizeof(T); \
            \
            private: \
                const T &m_value; \
        }; \
    }

}

#if (CHAR_MIN < 0)
DECLARE_NUMERIC_TYPE_INFO(char, true, CHAR_MIN, CHAR_MIN + 1, CHAR_MAX)
#else
DECLARE_NUMERIC_TYPE_INFO(char, true, CHAR_MAX, 0, CHAR_MAX - 1)
#endif

DECLARE_NUMERIC_TYPE_INFO(signed char, true, SCHAR_MIN, SCHAR_MIN + 1, SCHAR_MAX)
DECLARE_NUMERIC_TYPE_INFO(unsigned char, true, UCHAR_MAX, 0, UCHAR_MAX - 1)

DECLARE_NUMERIC_TYPE_INFO(short, true, SHRT_MIN, SHRT_MIN + 1, SHRT_MAX)
DECLARE_NUMERIC_TYPE_INFO(unsigned short, true, USHRT_MAX, 0, USHRT_MAX - 1)

DECLARE_NUMERIC_TYPE_INFO(int, true, INT_MIN, INT_MIN + 1, INT_MAX)
DECLARE_NUMERIC_TYPE_INFO(unsigned int, true, UINT_MAX, 0 , UINT_MAX - 1)

DECLARE_NUMERIC_TYPE_INFO(long, true, LONG_MIN, LONG_MIN + 1, LONG_MAX)
DECLARE_NUMERIC_TYPE_INFO(unsigned long, true, ULONG_MAX, 0, ULONG_MAX - 1)

#ifndef LLONG_MIN
#define LLONG_MIN (-__LONG_LONG_MAX__ - 1)
#endif

#ifndef LLONG_MAX
#define LLONG_MAX __LONG_LONG_MAX__
#endif

#ifndef ULLONG_MAX
#define ULLONG_MAX (__LONG_LONG_MAX__ * 2ULL + 1)
#endif

DECLARE_NUMERIC_TYPE_INFO(long long, true, LLONG_MIN, LLONG_MIN + 1, LLONG_MAX)
DECLARE_NUMERIC_TYPE_INFO(unsigned long long, true, ULLONG_MAX, 0, ULLONG_MAX - 1)

DECLARE_NUMERIC_TYPE_INFO(double, false, NAN, -DBL_MAX, DBL_MAX)
DECLARE_NUMERIC_TYPE_INFO(float, false, NAN, -FLT_MAX, FLT_MAX)
