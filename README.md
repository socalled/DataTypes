# SoCalled Data Types

Convenient tools to work with data types for Arduino and the ESP.
Is a part of the [SoCalled Framework](https://socalled.link).

**[SoCalled DataTypes](https://socalled.link/module/DataTypes)**
provides macros and templates for getting information about data types
which are convenient to use in template structures, classes and algorithms.

## License

The source code is provided under [Mozilla Public License Version 2.0](LICENSE.MPL-2.0.md)
that allows to use it, but requires to publish the changes made.

## Includes

*	**[SoCalledTypeInfo.h](src/SoCalledTypeInfo.h)**
	provides `TypeInfo<T>` class and macro wrappers for it.
	
## Build specifics

The library uses template operations that are available since C++17.
This should not be a problem in the latest versions of the ESP8266 tools.
However, messages may appear for Arduino and other chips:

*	`warning: variable templates only available with -std=c++14 or -std=gnu++14`
*	`error: invalid use of template-name 'SoCalled::TypeInfo' without an argument list`

It is required to specify a newer version of the C++ to the compiler.
To do this, change the argument `-std=…` to `-std=gnu++17` or newer
of the `compiler.cpp.flags` field in the
[platform.txt](https://arduino.github.io/arduino-cli/0.22/platform-specification/#platformtxt).

## Examples

*	**[NullValues](examples/NullValues)**
	shows how the null values are perceived.

	*	ESP8266 output:

		```
		Type               Value           Null           Is null
		nullptr_t          0               0              1
		void *             0               0              1
		int *              0               0              1
		DeclaredClass *    0               0              1
		DeclaredClass *    0x3ffef4ec      0              0
		*                  0               0              1
		*                  0x3ffef4f4      0              0
		signed char        0               -128           0
		unsigned char      0               255            0
		char               0               255            0
		unsigned char      0               255            0
		short              0               -32768         0
		unsigned short     0               65535          0
		unsigned short     0               65535          0
		int                0               -2147483648    0
		unsigned int       0               4294967295     0
		long               0               -2147483648    0
		unsigned long      0               4294967295     0
		float              0.000000e+00    nan            0
		double             0.000000e+00    nan            0
		```

*	**[NumericTypesInfo](examples/NumericTypesInfo)**
	shows how to get information about the fundamental numeric types,
	including the value taken as null,
	the minimum and maximum of the remaining ones.

	*	ESP8266 output:

		```
		Type                  Size    Null                    Min                     Max
		signed char           1       -128                    -127                    127
		unsigned char         1       255                     0                       254
		char                  1       255                     0                       254
		unsigned char         1       255                     0                       254
		short                 2       -32768                  -32767                  32767
		unsigned short        2       65535                   0                       65534
		unsigned short        2       65535                   0                       65534
		int                   4       -2147483648             -2147483647             2147483647
		unsigned int          4       4294967295              0                       4294967294
		long                  4       -2147483648             -2147483647             2147483647
		unsigned long         4       4294967295              0                       4294967294
		long long             8       -9223372036854775808    -9223372036854775807    9223372036854775807
		unsigned long long    8       18446744073709551615    0                       18446744073709551614
		float                 4       nan                     -3.402823e+38           3.402823e+38
		double                8       nan                     -1.797693e+308          1.797693e+308
		```

*	**[NumericValuesTypeInfo](examples/NumericValuesTypeInfo)**
	shows how to get information about numeric types
	based on variable values.

	*	ESP8266 output:

		```
		Type                  Size    Null                    Min                     Max
		signed char           1       -128                    -127                    127
		unsigned char         1       255                     0                       254
		char                  1       255                     0                       254
		unsigned char         1       255                     0                       254
		short                 2       -32768                  -32767                  32767
		unsigned short        2       65535                   0                       65534
		unsigned short        2       65535                   0                       65534
		int                   4       -2147483648             -2147483647             2147483647
		unsigned int          4       4294967295              0                       4294967294
		long                  4       -2147483648             -2147483647             2147483647
		unsigned long         4       4294967295              0                       4294967294
		long long             8       -9223372036854775808    -9223372036854775807    9223372036854775807
		unsigned long long    8       18446744073709551615    0                       18446744073709551614
		float                 4       nan                     -3.402823e+38           3.402823e+38
		double                8       nan                     -1.797693e+308          1.797693e+308
		```

*	**[PointerTypesInfo](examples/PointerTypesInfo)**
	shows what properties the pointer types have.

	*	ESP8266 output:

		```
		Type               Size    Null    Is pointer    Is fundamental
		nullptr_t          4       0       1             1
		void *             4       0       1             0
		int *              4       0       1             0
		DeclaredClass *    4       0       1             0
		*                  4       0       1             0
		```

	*	ATmega168 output:

		```
		Type               Size    Null    Is pointer    Is fundamental
		nullptr_t          2       0       1             1
		void *             2       0       1             0
		int *              2       0       1             0
		DeclaredClass *    2       0       1             0
		*                  2       0       1             0
		```

*	**[PointerValuesTypeInfo](examples/PointerValuesTypeInfo)**
	shows how to get information about pointer types
	based on variable values.

	*	ESP8266 output:

		```
		Type               Size    Value         Null    Is pointer    Is fundamental
		nullptr_t          4       0             0       1             1
		void *             4       0x3ffef65c    0       1             0
		int *              4       0x3ffef664    0       1             0
		DeclaredClass *    4       0x3ffef66c    0       1             0
		*                  4       0x3ffef674    0       1             0
		```

	*	ATmega168 output:

		```
		Type               Size    Value         Null    Is pointer    Is fundamental
		nullptr_t          2       0             0       1             1
		void *             2       0x468         0       1             0
		int *              2       0x46c         0       1             0
		DeclaredClass *    2       0x470         0       1             0
		*                  2       0x474         0       1             0
		```
